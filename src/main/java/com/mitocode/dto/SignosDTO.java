package com.mitocode.dto;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

public class SignosDTO {

	private Integer idSignos;

	@NotNull
	private PacienteDTO paciente;

	@NotNull
	private LocalDateTime fecha;

	@NotNull
	private Double temperatura;

	@NotNull
	private Double pulso;

	@NotNull
	private Double ritmoRespiratorio;

	public Integer getIdSignos() {
		return idSignos;
	}

	public void setIdSignos(Integer idSignos) {
		this.idSignos = idSignos;
	}

	public PacienteDTO getPaciente() {
		return paciente;
	}

	public void setPaciente(PacienteDTO paciente) {
		this.paciente = paciente;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public Double getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(Double temperatura) {
		this.temperatura = temperatura;
	}

	public Double getPulso() {
		return pulso;
	}

	public void setPulso(Double pulso) {
		this.pulso = pulso;
	}

	public Double getRitmoRespiratorio() {
		return ritmoRespiratorio;
	}

	public void setRitmoRespiratorio(Double ritmoRespiratorio) {
		this.ritmoRespiratorio = ritmoRespiratorio;
	}

}
