package com.mitocode;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

//@Configuration
//Clase Opcional, si el Bcrypt ya esta en la clase SecurityConfig usar static, sino usar una clase aparte
public class BcryptConfig {
	
	//@Bean 	
	//https://stackoverflow.com/questions/70254555/spring-boot-2-6-0-error-creating-bean-with-name-websecurityconfig
	public BCryptPasswordEncoder passwordEnconder() {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();		
		return bCryptPasswordEncoder;
	}

}
