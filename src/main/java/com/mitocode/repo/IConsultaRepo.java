package com.mitocode.repo;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.mitocode.model.Consulta;

public interface IConsultaRepo extends IGenericRepo<Consulta, Integer>{
	
	//JAIME toLowercase() jaime
	//jaime lower() jaime
	//JPQL Java Persistence Query Language  //MicroStream Investiguen*
	@Query("FROM Consulta c WHERE c.paciente.dni = :dni OR LOWER(c.paciente.nombres) LIKE %:nombreCompleto% OR LOWER(c.paciente.apellidos) LIKE %:nombreCompleto%")
	List<Consulta> buscar(@Param("dni") String dni, String nombreCompleto);
	
	// >=19-04-2022  <25-04-2022
	@Query("FROM Consulta c WHERE c.fecha BETWEEN :fechaConsulta1 AND :fechaConsulta2")
	List<Consulta> buscarFecha(@Param("fechaConsulta1") LocalDateTime fechaConsulta1,  @Param("fechaConsulta2") LocalDateTime fechaConsulta2);

	@Query(value = "select * from fn_listarResumen()", nativeQuery = true)
	List<Object[]> listarResumen();
	
	//[5,	"02/04/2022"] x[0] = 5 x[1] = "02/04/2022"
	//[2,	"23/04/2022"]
	//[1,	"24/04/2022"]
}
